<!doctype html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Basic page needs ================================================== -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="google-site-verification" content="q7avzIpl-hW36h-rrLGXhKvMPqJMZ3ZvibZZFAfA7BE" />
  {% if settings.favicon_enable %}
  <link rel="shortcut icon" href="{{ 'shop_favicon.png' | asset_url }}" type="image/png" />
  {% endif %}

  <!-- Title and description ================================================== -->
  <title>
  {{ page_title }}{% if current_tags %}{% assign meta_tags = current_tags | join: ', ' %} &ndash; {{ 'general.meta.tags' | t: tags: meta_tags }}{% endif %}{% if current_page != 1 %} &ndash; {{ 'general.meta.page' | t: page: current_page }}{% endif %}{% unless page_title contains shop.name %} &ndash; {{ shop.name }}{% endunless %}
  </title>

  {% if page_description %}
  <meta name="description" content="{{ page_description | escape }}">
  {% endif %}

  <!-- Helpers ================================================== -->
  {% include 'social-meta-tags' %}
  <link rel="canonical" href="{{ canonical_url }}">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="theme-color" content="{{ settings.main_color }}">
  
  <!-- FONTS ================================================== -->
  <link href='//fonts.googleapis.com/css?family={{ settings.heading_font_family }}:400,700,600,500,300' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family={{ settings.body_font_family }}:400,700,600,500,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

  <!-- CSS ================================================== -->
  {{ 'bootstrap.min.css' | asset_url | stylesheet_tag }}
  {{ 'style.scss.css' | asset_url | stylesheet_tag }}
  {{ 'color.scss.css' | asset_url | stylesheet_tag }}
  {{ 'navigation.scss.css' | asset_url | stylesheet_tag }}
  {{ 'settings.scss.css' | asset_url | stylesheet_tag }}

  <!-- Header hook for plugins ================================================== -->
  {{ content_for_header }}
  
  <!--[if lt IE 9]>
    {{ 'html5shiv.js' | asset_url | script_tag }}
    {{ 'respond.min.js' | asset_url | script_tag }}
  <![endif]-->

  {{ '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js' | script_tag }}
  {{ 'modernizr.min.js' | asset_url | script_tag }}

  {% comment %}
    If you store has customer accounts disabled, you can remove the following JS file
  {% endcomment %}
  {% if template contains 'customers' %}
    {{ 'shopify_common.js' | shopify_asset_url | script_tag }}
  {% endif %}
<script type="text/javascript">
(function(a,e,c,f,g,h,b,d){var k={ak:"856431196",cl:"48M_CKz5gHAQ3LSwmAM",autoreplace:"(209) 777-2155"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[g]||(a[g]=k.ak);b=e.createElement(h);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(h)[0];d.parentNode.insertBefore(b,d);a[f]=function(b,d,e){a[c](2,b,k,d,null,new Date,e)};a[f]()})(window,document,"_googWcmImpl","_googWcmGet","_googWcmAk","script");
</script>
  
</head>

{% comment %}
  Add the page template as a class for easy page or template specific styling.
{% endcomment %}
<body id="{{ page_title | handle }}" class="{% if customer %}customer-logged-in {% endif %}template-{{ template | replace: '.', ' ' | truncatewords: 1, '' | handle }}" >
  
  <!-- Site Loader -->
  <div class="loader">
    <div class="spinner"></div>
  </div>

  <!-- Main wrapper -->
  <div class="main-wrapper">
    
    {% if page.handle == "home-slider" %}
    	{% include 'navigation2' %}
    {% else %}
    	{% include 'navigation' %}
    {% endif %}
    
    {% if template contains 'index' %}
    	{% include 'home-header' %}
    {% endif %}
    
    {% if settings.map_show %}
      {% if template contains 'contact' %}
          {% include 'contact-header' %}
      {% else %}
          {% unless template contains 'index' %}
              {% include 'secundary-header' %}
          {% endunless %}
      {% endif %}
    {% else %}
    	{% unless template contains 'index' %}
            {% include 'secundary-header' %}
        {% endunless %}
    {% endif %}
    

    {{ content_for_layout }}{% include 'vnavs' %}
    
    {% unless template contains 'customers' %}
    	{% if settings.newsletter_show %}
    		{% include 'newsletter' %}
    	{% endif %}
    {% endunless %}
    
    {% include 'footer' %}

  </div>

  {{ 'handlebars.min.js' | asset_url | script_tag }}
  {{ 'bootstrap.min.js' | asset_url | script_tag }}
  {{ 'fastclick.js' | asset_url | script_tag }}
  {{ 'enquire.min.js' | asset_url | script_tag }}
  {{ 'stellar.min.js' | asset_url | script_tag }}
  {{ 'owl-carousel.min.js' | asset_url | script_tag }}
  {{ 'instafeed.min.js' | asset_url | script_tag }}
  {{ 'simplyscroll.js' | asset_url | script_tag }}
  {{ 'jquery.ajaxchimp.js' | asset_url | script_tag }}
  {{ 'jquery.ajaxchimp.js' | asset_url | script_tag }}
  {{ 'responsive-nav.js' | asset_url | script_tag }}
  {{ 'ease.js' | asset_url | script_tag }}
  {{ 'scripts.js' | asset_url | script_tag }}
<script type="text/javascript">
  (function(){
    if(window.location.href.indexOf('/pages/contact-us/?contact_posted=true')!=-1)
    {
      (new Image()).src="//www.googleadservices.com/pagead/conversion/856431196/?label=T5Y4COnv528Q3LSwmAM&guid=ON&script=0";
    }
  })();
</script>
  <!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 856431196;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/856431196/?guid=ON&amp;script=0"/>
</div>
</noscript>

</body>
</html>
